const Pool = require('pg').Pool;
const pool = new Pool({
  user: 'david',
  host: 'localhost',
  database: 'api',
  password: 'password',
  port: 5432
});

const getEmployees = (request, response) => {
  pool.query('SELECT * FROM employees ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};

const addEmployee = (request, response) => {
  const {
    name,
    code,
    profession,
    color,
    city,
    branch,
    assigned
  } = request.body;

  pool.query(
    'INSERT INTO employees (name, code, profession, color, city, branch, assigned) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *',
    [name, code, profession, color, city, branch, assigned],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(201).send(results.rows[0]);
    }
  );
};

const updateEmployee = (request, response) => {
  const id = parseInt(request.params.id);
  const { field, value } = request.body;

  pool.query(
    `UPDATE employees SET ${field} = $1 WHERE id = $2 RETURNING *`,
    [value, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).send(results.rows[0]);
    }
  );
};

const deleteEmployee = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query('DELETE FROM employees WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).send({ id });
  });
};

module.exports = {
  getEmployees,
  addEmployee,
  updateEmployee,
  deleteEmployee
};
