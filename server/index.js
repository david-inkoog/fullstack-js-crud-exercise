const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const db = require('./queries');

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
};

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.get('/api/employees', cors(corsOptions), function(req, res) {
  db.getEmployees(req, res);
});

app.put('/api/employee/:id', cors(corsOptions), function(req, res) {
  db.updateEmployee(req, res);
});

app.post('/api/employee', cors(corsOptions), function(req, res) {
  db.addEmployee(req, res);
});

app.delete('/api/employee/:id', cors(corsOptions), function(req, res) {
  db.deleteEmployee(req, res);
});

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'));
