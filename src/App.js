import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';
import { EmployeeActions } from './store/actionCreators';
import { GET_EMPLOYEES } from './store/modules/employee';
import EditableTextBox from './components/EditableTextBox';
import Modal from './components/Modal';
import LoadingSpinner from './components/LoadingSpinner';

import 'react-table/react-table.css';
import './App.css';

class App extends Component {
  state = {
    modalOpen: false
  };
  componentDidMount() {
    EmployeeActions.getEmployees();
  }

  handleOpenModal = () => {
    this.setState({
      modalOpen: true
    });
  };

  handleCloseModal = () => {
    this.setState({
      modalOpen: false
    });
  };

  handleDelete = id => {
    EmployeeActions.deleteEmployee(id);
  };

  handleSubmit = form => {
    EmployeeActions.addEmployee(form);
  };

  render() {
    const { employees, loading } = this.props;

    const columns = [
      {
        Header: 'Id',
        accessor: 'id'
      },
      {
        Header: 'Name',
        accessor: 'name',
        Cell: row => {
          return (
            <EditableTextBox
              initialValue={row.value}
              save={value =>
                EmployeeActions.updateEmployee(row.original.id, 'name', value)
              }
            />
          );
        }
      },
      {
        Header: 'Code',
        accessor: 'code',
        Cell: row => {
          return (
            <EditableTextBox
              initialValue={row.value}
              save={value =>
                EmployeeActions.updateEmployee(row.original.id, 'code', value)
              }
            />
          );
        }
      },
      {
        Header: 'Profession',
        accessor: 'profession',
        Cell: row => {
          return (
            <EditableTextBox
              initialValue={row.value}
              save={value =>
                EmployeeActions.updateEmployee(
                  row.original.id,
                  'profession',
                  value
                )
              }
            />
          );
        }
      },
      {
        Header: 'Color',
        accessor: 'color',
        Cell: row => (
          <div
            className="color-background"
            style={{
              backgroundColor: row.value
            }}
          >
            <span className="color-text">
              {' '}
              <EditableTextBox
                initialValue={row.value}
                save={value =>
                  EmployeeActions.updateEmployee(
                    row.original.id,
                    'color',
                    value
                  )
                }
              />
            </span>
          </div>
        )
      },
      {
        Header: 'City',
        accessor: 'city',
        Cell: row => {
          return (
            <EditableTextBox
              initialValue={row.value}
              save={value =>
                EmployeeActions.updateEmployee(row.original.id, 'city', value)
              }
            />
          );
        }
      },
      {
        Header: 'Branch',
        accessor: 'branch',
        Cell: row => {
          return (
            <EditableTextBox
              initialValue={row.value}
              save={value =>
                EmployeeActions.updateEmployee(row.original.id, 'branch', value)
              }
            />
          );
        }
      },
      {
        Header: 'Assigned',
        accessor: 'assigned',
        Cell: row => (
          <span>
            <span
              style={{
                color: row.value ? '#57d500' : '#ff2e00',
                transition: 'all .5s ease'
              }}
            >
              &#x25cf;
            </span>{' '}
            {row.value ? 'Assigned' : 'Not Assigned'}
          </span>
        )
      },
      {
        Header: 'Delete',
        accessor: 'id',
        Cell: row => (
          <button
            className="btnDelete"
            onClick={() => this.handleDelete(row.original.id)}
          >
            Delete
          </button>
        )
      }
    ];

    return loading ? (
      <LoadingSpinner />
    ) : (
      <div className="App">
        {this.state.modalOpen ? (
          <div onClick={() => this.closeModalHandler()} className="back-drop" />
        ) : null}

        <h1>Plexxis Employees</h1>
        <button className="btnAdd" onClick={() => this.handleOpenModal()}>
          ADD
        </button>
        <ReactTable data={employees} columns={columns} />
        <Modal
          isOpen={this.state.modalOpen}
          close={this.handleCloseModal}
          onSubmit={form => this.handleSubmit(form)}
        />
      </div>
    );
  }
}

export default connect(({ employee, pender }) => ({
  employees: employee.employees,
  loading: pender.pending[GET_EMPLOYEES]
}))(App);
