import { combineReducers } from 'redux';
import { penderReducer } from 'redux-pender';
import employee from './employee';

export default combineReducers({
  employee,
  pender: penderReducer
});
