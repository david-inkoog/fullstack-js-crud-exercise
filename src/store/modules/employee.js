import { createAction, handleActions } from 'redux-actions';
import { applyPenders } from '../../lib/common';
import * as EmployeeAPI from '../../lib/api/employee';

// Types
export const GET_EMPLOYEES = 'employee/GET_EMPLOYEES';
export const ADD_EMPLOYEE = 'employee/ADD_EMPLOYEE';
export const UPDATE_EMPLOYEE = 'employee/UPDATE_EMPLOYEE';
export const DELETE_EMPLOYEE = 'employee/DELETE_EMPLOYEE';

// Create Actions with API function
export const getEmployees = createAction(
  GET_EMPLOYEES,
  EmployeeAPI.getEmployees
);
export const addEmployee = createAction(ADD_EMPLOYEE, EmployeeAPI.addEmployee);
export const updateEmployee = createAction(
  UPDATE_EMPLOYEE,
  EmployeeAPI.updateEmployee
);
export const deleteEmployee = createAction(
  DELETE_EMPLOYEE,
  EmployeeAPI.deleteEmployee
);

const initialState = {
  employees: []
};

const reducer = handleActions({}, initialState);

export default applyPenders(reducer, [
  {
    type: GET_EMPLOYEES,
    onPending: state => {
      return {
        ...state
      };
    },
    onSuccess: (state, action) => {
      return {
        ...state,
        employees: action.payload.data
      };
    },
    onFailure: (state, action) => {
      return {
        ...state,
        error: action.payload.data
      };
    }
  },
  {
    type: ADD_EMPLOYEE,
    onPending: state => {
      return {
        ...state
      };
    },
    onSuccess: (state, action) => {
      return {
        ...state,
        employees: [...state.employees, action.payload.data]
      };
    },
    onFailure: (state, action) => {
      return {
        ...state,
        error: action.payload.data
      };
    }
  },
  {
    type: UPDATE_EMPLOYEE,
    onPending: state => {
      return {
        ...state
      };
    },
    onSuccess: (state, action) => {
      const updatedEmployees = state.map(item => {
        if (item.id === action.id) {
          return action.payload.data;
        }
        return item;
      });

      return {
        ...state,
        employees: updatedEmployees
      };
    },
    onFailure: (state, action) => {
      return {
        ...state,
        error: action.payload.data
      };
    }
  },
  {
    type: DELETE_EMPLOYEE,
    onPending: state => {
      return {
        ...state
      };
    },
    onSuccess: (state, action) => {
      return {
        ...state,
        employees: state.employees.filter(
          element => element.id !== action.payload.data.id
        )
      };
    },
    onFailure: (state, action) => {
      return {
        ...state,
        error: action.payload.data
      };
    }
  }
]);
