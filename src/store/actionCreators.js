import { bindActionCreators } from 'redux';

import * as employeeActions from './modules/employee';

import store from './index';

const { dispatch } = store;

export const EmployeeActions = bindActionCreators(employeeActions, dispatch);
