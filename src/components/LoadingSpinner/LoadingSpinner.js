import React from 'react';
import './LoadingSpinner.css';

const LoadingSpinner = () => {
  return (
    <div className="loadingPage">
      <div className="loadingBlock">
        <div className="loading" />
      </div>
    </div>
  );
};

export default LoadingSpinner;
