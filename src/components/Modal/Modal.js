import React, { Component } from 'react';

import './Modal.css';

class Modal extends Component {
  initialState = {
    name: '',
    code: '',
    profession: '',
    color: '',
    city: '',
    branch: '',
    assigned: false
  };
  state = this.initialState;

  isFormValid = () => {
    const { name, code, profession, color, city, branch } = this.state;
    let isGood = true;

    if (!name || !code || !profession || !color || !city || !branch)
      isGood = false;

    return isGood;
  };

  handleSubmit = () => {
    if (this.isFormValid()) {
      let form = {
        ...this.state
      };
      this.props.onSubmit(form);
      this.setState(this.initialState);
      this.props.close();
    }
  };

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    const { isOpen, close } = this.props;
    const {
      name,
      code,
      profession,
      color,
      city,
      branch,
      assigned
    } = this.state;
    return (
      <React.Fragment>
        {isOpen ? (
          <React.Fragment>
            <div className="Modal-overlay" onClick={close} />
            <div className="Modal">
              <p className="title">ADD EMPLOYEE</p>
              <div className="content">
                <form onSubmit={this.handleSubmit}>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="Name"
                      name="name"
                      value={name}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="Code"
                      name="code"
                      value={code}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="Profession"
                      name="profession"
                      value={profession}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="Color"
                      name="color"
                      value={color}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="City"
                      name="city"
                      value={city}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="input-field">
                    <input
                      type="text"
                      className="text-input"
                      placeholder="Branch"
                      name="branch"
                      value={branch}
                      onChange={this.handleInputChange}
                    />
                  </div>
                  <div className="check-boxes">
                    <label>
                      Assign{' '}
                      <input
                        type="checkbox"
                        name="assigned"
                        onChange={this.handleInputChange}
                        checked={assigned}
                      />{' '}
                    </label>
                  </div>
                </form>
              </div>
              <div className="button-wrap">
                <button onClick={() => this.handleSubmit()}> SAVE </button>
              </div>
            </div>
          </React.Fragment>
        ) : null}
      </React.Fragment>
    );
  }
}

export default Modal;
