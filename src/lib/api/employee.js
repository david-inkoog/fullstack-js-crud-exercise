import axios from 'axios';

export const getEmployees = () => axios.get('/api/employees');

export const addEmployee = form => axios.post('/api/employee', form);

export const updateEmployee = (id, field, value) =>
  axios.put(`/api/employee/${id}`, { id, field, value });

export const deleteEmployee = id => axios.delete(`/api/employee/${id}`);
