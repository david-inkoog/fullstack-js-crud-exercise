# Plexxis Interview Exercise

## Redux (ducks style)

- library - redux, react-redux, redux-actions, redux-pender

## Components

- EditableTextBox: Update data by clicking table
- LoadingSpinner: Show loading spinner while app is loading
- Modal: Modal for adding employee

## Axios

- Using axios library to call api

## PostgresSQL

- REST API with PostgreSQL

## React Table

- Use React-Table default design

## Postgresql setup

1. `brew install postgresql`
2. `brew services start postgresql`
3. `psql postgres`
4. `CREATE ROLE david WITH LOGIN PASSWORD 'password';`
5. `ALTER ROLE me CREATEDB;`
6. `psql -d postgres -U david`
7. `CREATE DATABASE api;`
8. `\c api`
9. `CREATE TABLE employees ( ID SERIAL PRIMARY KEY, name VARCHAR(50), code VARCHAR(50), profession VARCHAR(150), color VARCHAR(50), city VARCHAR(50), branch VARCHAR(50), assigned boolean );`

10. `INSERT INTO employees (name, code, profession, color, city, branch, assigned) VALUES ('Kyle Lowry', 'F100', 'Drywall Installer', '#FF6600', 'Brampton', 'Abacus', true), ('DeMar DeRozan','F101','Drywall Installer','yellow','Brampton','Pillsworth', false)`
